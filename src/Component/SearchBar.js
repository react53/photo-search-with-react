import React from "react";


class SearchBar extends React.Component {
state = {txt : ""}


// {/* تابعی برای اینکه زمانی که اینپوت تغیر کرد در کنسول مقدار تغیرات درون این\وت را نمایش دهد */}
  // oninputChange(e){
  //   console.log(e.target.value);
  // }


// {/* تابعی برای زمانی که رویه چیزی کلیک شد در کنسول نمایش دهد که شما کلیک کردید */}
  // oninputClick(e){
  //   console.log("you click in btn Send one !!");
  // }

// {/* تابعی برای ارسال مقدار درون اینپوت با استفاده از کلید اینتر */}

  submitenter = (event) =>{
    event.preventDefault();
    this.props.onSubmit(this.state.txt);
    console.log(this.state.txt);
  }



  render(){
    return(
      <div>
        <form onSubmit={this.submitenter}>
          <div className="ui search">
            <div className="ui icon input">
              <input className="prompt" value={this.state.txt} onChange={e =>this.setState({txt :e.target.value})} type="text" placeholder="Search"  />
              <i className="search icon"></i>
            </div>
          </div>
        </form>
          <br/>
        <h4>you search : {this.state.txt}</h4>
{/* نحوه استفاده از ایونت ها در ری اکت اسم های تابع ها میتواند دلخواه باشد */}
{/*         <button className="ui inverted red button" onClick={this.oninputClick} >Send one</button>     */}
{/* نحوه استفاده درون خطی ایونت ها در ری اکت  */}
{/*         <button className="ui inverted red button" onClick={(e)=>{console.log("you click in btn Send tow !!");}} >Send tow</button>    */}
{/* نحوه ست استست کردن درون خطی با استفاده از ایونت ان چینج  */}
{/*         <input className="prompt" type="text" value={this.state.txt} onChange={e =>this.setState({txt :e.target.value})}/>    */}

      </div>
    );
  };
};

export default SearchBar;
